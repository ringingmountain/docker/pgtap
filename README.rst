=====
pgTap
=====

A Docker image for running `pgTap`_ tests against a PostgreSQL database.

.. csv-table::
   :widths: 10, 30

   "Repository",    "https://gitlab.com/ringingmountain/docker/pgtap"
   "CI Pipeline",   "https://gitlab.com/ringingmountain/docker/pgtap/pipelines"
   "Documentation", "http://pgtap-docker.readthedocs.io/"


|pipeline| |docs|


Usage
-----

.. code-block:: bash

   docker run -e PGPASSWORD=secret ringingmountain/pgtap:latest [-h HOSTNAME] [-p PORT] [-U USERNAME] [-t TESTS]


Quickstart Development Guide
----------------------------

Build Documentation
~~~~~~~~~~~~~~~~~~~

.. code-block:: bash

   ci/docs.sh


Build Docker Image
~~~~~~~~~~~~~~~~~~

.. code-block:: bash

   ci/build.sh [TAG]


Run Gitlab CI Jobs Locally
~~~~~~~~~~~~~~~~~~~~~~~~~~

You will first need to install the `Gitlab runner`_ package and `register`_ a runner on your local machine.

.. code-block:: bash

   gitlab-runner exec docker <job_name> --docker-services docker:dind --docker-privileged


Releasing a New Version
~~~~~~~~~~~~~~~~~~~~~~~

1. Checkout master.
2. Decide whether you are releasing a major, minor, or patch revision.
   For assistance in making this choice see the `SemVer`_ standard.
3. Ensure an entry for the version exists in ``CHANGELOG.rst`` summarizing the changes you are releasing.
4. Update the version in docs/conf.py
5. Commit the changes, commenting that you are bumping the version.
6. Tag the repo with the matching version.
7. Push to the central remote and your fork.



 .. |pipeline| image:: https://gitlab.com/ringingmountain/docker/pgtap/badges/master/pipeline.svg
                :target: https://gitlab.com/ringingmountain/docker/pgtap/pipelines
..  |docs|     image:: https://readthedocs.org/projects/pgtap-docker/badge/?version=latest
                :target: https://pgtap-docker.readthedocs.io/

.. _pgTap: https://pgtap.org/
.. _Gitlab runner: https://docs.gitlab.com/runner/install/
.. _register: https://docs.gitlab.com/runner/register/index.html#one-line-registration-command
.. _SemVer: https://semver.org
