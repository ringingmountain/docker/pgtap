Changelog
=========

`Next Release`_
---------------

`0.0.4`_ (2020-02-16)
---------------------

* Run as non-root user
* Make test script exit on failure
* Improve exit clause of test script

`0.0.3`_ (2020-02-16)
---------------------

* Fix script name

`0.0.2`_ (2020-02-14)
---------------------

* Fix build
* Update to pgTap 1.1.0

`0.0.1`_ (2020-01-26)
---------------------

* Updates to CI pipeline and docs
* Update CLI logging utils

`0.0.0`_ (2020-01-26)
---------------------

* Initial Release


.. _Next Release: https://gitlab.com/ringingmountain/docker/pgtap/compare/0.0.4...HEAD
.. _0.0.4: https://gitlab.com/ringingmountain/docker/pgtap/compare/0.0.3...0.0.4
.. _0.0.3: https://gitlab.com/ringingmountain/docker/pgtap/compare/0.0.2...0.0.3
.. _0.0.2: https://gitlab.com/ringingmountain/docker/pgtap/compare/0.0.1...0.0.2
.. _0.0.1: https://gitlab.com/ringingmountain/docker/pgtap/compare/0.0.0...0.0.1
.. _0.0.0: https://gitlab.com/ringingmountain/docker/pgtap/-/tags/0.0.0
