#!/usr/bin/env sh
set -e

# Colors
export COLOR_RESET="\033[0m"
export COLOR_GREY="\033[1;30m"
export COLOR_RED="\033[0;31m"
export COLOR_GREEN="\033[0;32m"
export COLOR_YELLOW="\033[0;33m"
export COLOR_BLUE="\033[1;34m"

export CI_REGISTRY=docker.io
export CI_REGISTRY_IMAGE=index.docker.io/ringingmountain/pgtap

# CI_COMMIT_SHA is set by Gitlab CI, defaulting it allows running scripts outside of Gitlab CI
export CI_COMMIT_SHA="${CI_COMMIT_SHA:-$(git rev-parse HEAD | cut -b 1-8)}"

if test -n "${CI_COMMIT_TAG}"
then
  DOCKER_TAG="${CI_COMMIT_TAG}"
else
  DOCKER_TAG="$(echo "${CI_COMMIT_SHA}" | cut -b 1-8)"
fi
LATEST="${CI_REGISTRY_IMAGE}:latest"
TAG="${CI_REGISTRY_IMAGE}:${DOCKER_TAG}"

log_run() {
  log_message "${1}"
  shift

  QUIET="${QUIET:-0}"

  if test "${QUIET}" -eq 2
  then
    "$@" >/dev/null 2>&1
  elif test "${QUIET}" -eq 1
  then
    "$@" >/dev/null
  else
    "$@"
  fi
  RET=$?

  if test ${RET} -eq 0
  then
    log_success
  else
    log_failure
  fi

  return ${RET}
}

_print_msg() {
  if test "${QUIET:-0}" -ge 1
  then
    printf "%b%s%b ... " "${COLOR}" "$*" "${COLOR_RESET}"
  else
    printf "%b%s%b\n" "${COLOR}" "$*" "${COLOR_RESET}"
  fi
}

log_message() {
  COLOR=${COLOR_BLUE} _print_msg "$*"
}

log_warning() {
  COLOR=${COLOR_YELLOW} _print_msg "$*"
}

log_success() {
  QUIET=0 COLOR=${COLOR_GREEN} _print_msg "${*:-ok}"
}

log_failure() {
  QUIET=0 COLOR=${COLOR_RED} _print_msg "${*:-failed}"
}

log_exit() {
  CODE=$?
  test ${CODE} -ne 0 && log_failure "exit ${CODE}"
  exit ${CODE}
}

trap log_exit EXIT
