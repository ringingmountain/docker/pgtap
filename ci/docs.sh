#!/usr/bin/env sh
set -e

. ci/common.sh

log_run 'Installing dependencies' pip install -q Sphinx

log_run 'Building documentation' sphinx-build docs build/docs
