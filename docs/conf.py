project = 'pgTap'
copyright = '2019 Ringing Mountain, LLC'
author = 'Robin Klingsberg'

version = '0.0.4'

source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

htmlhelp_basename = "pgtap_doc"
